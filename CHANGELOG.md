# Change Log for pkg-builder

## [0.1.2] - Skip build and test

### Added
- option to skip test and build, but still including the files in the zip-archives
- Changelog file

## [0.1.1] - Substitute current directory in args

### Added
- String values in args of test and build can contain `!CD!`, which will be replace with the current working directory.

## [0.1.0] - Initial release

