use git2::{Delta, Deltas, DiffOptions, Repository};
use log::{error, info};
use walkdir::WalkDir;
use zip::{result::ZipError, write::FileOptions};

use std::{
    env,
    fs::{self, OpenOptions},
    io::{Read, Result, Write},
    path::{Path, PathBuf},
};

use crate::config::FileInfo;

/// Store info to copy one file to another
#[derive(Debug, Clone)]
pub struct FileCopy {
    src: PathBuf,
    dst: PathBuf,
}

impl FileCopy {
    /// Copy the source file to the destination, creating parent directories if necessary.
    pub fn make_copy(&self, out: impl AsRef<Path>) -> Result<()> {
        self._make_copy(out.as_ref())
    }

    fn _make_copy(&self, out: &Path) -> Result<()> {
        let p = out.join(match self.dst.strip_prefix("/") {
            Ok(v) => v,
            Err(_) => &self.dst,
        });

        fs::create_dir_all(p.parent().unwrap())?;
        if !&self.src.is_dir() {
            match fs::copy(&self.src, p) {
                Ok(_) => Ok(()),
                Err(e) => Err(e),
            }
        } else {
            Ok(())
        }
    }
}

/// Stores a list of files to copy and the name of the package
pub struct PackageBuilder {
    files: Vec<FileCopy>,
    name: String,
}

impl PackageBuilder {
    /// Create a new PackageBuilder with the given name and
    /// process all the FileInfos.
    ///
    /// Globs will be matched against `pkg/` and all matches
    /// are added to the files of the created PackageBuilder.
    pub fn from(files: Vec<FileInfo>, name: String) -> Self {
        let mut copier = PackageBuilder {
            files: Vec::with_capacity(files.len()),
            name,
        };
        let cwd = env::current_dir().unwrap();
        let pkg = cwd.join("pkg/");
        let root = Path::new("/");

        for info in files {
            match info {
                FileInfo::Path { source, dest } => {
                    copier.files.push(FileCopy {
                        src: pkg.join(source),
                        dst: root.join(dest),
                    });
                }
                FileInfo::Glob(pattern) => {
                    for entry in
                        glob::glob(&format!("pkg/{}", &pattern)).expect("Failed to apply glob")
                    {
                        match entry {
                            Ok(path) => copier.files.push(FileCopy {
                                src: cwd.join(&path),
                                dst: root.join(path),
                            }),
                            Err(e) => error!("Error: {}", e),
                        }
                    }
                }
            }
        }

        copier
    }

    /// Drop all files that have not changed since the commit specified with the reference.
    pub fn filter_reference(&mut self, reference: &str) {
        println!("Filtering for changes since {}", reference);
        let repo = Repository::open(".").expect("Could not open git repository");

        let tree = repo
            .resolve_reference_from_short_name(reference)
            .expect("Reference does not exist.")
            .peel_to_tree();
        info!("{:?}", tree);

        let deltas = repo
            .diff_tree_to_workdir(
                tree.ok().as_ref(),
                Some(
                    DiffOptions::new()
                        .recurse_untracked_dirs(true)
                        .include_untracked(true)
                        .pathspec("pkg/"),
                ),
            )
            .expect("Could not get delta");

        self.apply_diff_filter(deltas.deltas());
    }

    fn apply_diff_filter(&mut self, deltas: Deltas) {
        let cwd = env::current_dir().unwrap();

        let paths_in_diff: Vec<PathBuf> = deltas
            .filter_map(|delta| {
                let new_file = delta.new_file();
                info!("{:?}", delta);
                if delta.status() != Delta::Unmodified && new_file.exists() {
                    new_file.path().map(|path| cwd.join(path))
                } else {
                    None
                }
            })
            .collect();
        self.files = self
            .files
            .clone()
            .into_iter()
            .filter(|p| paths_in_diff.contains(&p.src))
            .collect();
    }

    /// Add a new path to the files, path needs to be relative to the current working directory.
    pub fn add_path(&mut self, src: &Path, dst: &Path) {
        self.files.push(FileCopy {
            src: src.to_path_buf(),
            dst: dst.to_path_buf(),
        });
    }

    /// Start the build process
    pub fn build(&self, out: impl AsRef<Path>, prefix: Option<&str>) -> PathBuf {
        self._build(out.as_ref(), prefix)
    }

    fn _build(&self, out: &Path, prefix: Option<&str>) -> PathBuf {
        fs::create_dir_all(&out).expect("Failed to create output directory");
        let tmp_dir = self.copy_files();
        let out = self.package_dir(&tmp_dir, out, prefix).unwrap();
        self.clean_files(&tmp_dir).unwrap();
        out
    }

    fn copy_files(&self) -> PathBuf {
        let tmp_dir = env::temp_dir().join("pkg-builder").join(&self.name);
        println!("Copying files to {}", tmp_dir.display());
        fs::create_dir_all(&tmp_dir).unwrap();

        for file in &self.files {
            match file.make_copy(&tmp_dir) {
                Ok(()) => {
                    info!("Copied file: {}", file.src.display())
                }
                Err(v) => {
                    error!("Failed to copy: {}, {}", file.src.display(), v)
                }
            };
        }

        tmp_dir
    }

    fn package_dir(
        &self,
        tmp_dir: &Path,
        out: &Path,
        prefix: Option<&str>,
    ) -> zip::result::ZipResult<PathBuf> {
        let file_name = format!("{}{}.zip", prefix.unwrap_or(""), &self.name);
        let path = out.join(file_name);
        println!("Packaging zip file: {}.zip", path.display());
        match OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(&path)
        {
            Ok(file) => {
                let mut zip = zip::ZipWriter::new(file);
                let options = FileOptions::default();
                for entry in WalkDir::new(tmp_dir)
                    .into_iter()
                    .skip(1)
                    .filter_map(|e| e.ok())
                {
                    let path = entry
                        .path()
                        .strip_prefix(tmp_dir)
                        .unwrap()
                        .to_str()
                        .unwrap();

                    if entry.file_type().is_dir() {
                        info!("Creating dir {} in zip", path);
                        zip.add_directory(path, options)?;
                    } else {
                        let mut buf = Vec::new();
                        OpenOptions::new()
                            .read(true)
                            .open(entry.path())
                            .expect("Failed to open file")
                            .read_to_end(&mut buf)
                            .expect("Failed to read file");
                        zip.start_file(path, options)
                            .expect("Failed to start file in zip");
                        zip.write_all(buf.as_slice())
                            .expect("Failed to write in zip");
                    }
                }
                zip.finish()?;
                Ok(path)
            }
            Err(_) => Err(ZipError::FileNotFound),
        }
    }

    fn clean_files(&self, tmp_dir: &Path) -> Result<()> {
        println!("Cleaning tmp directory {}", tmp_dir.display());
        fs::remove_dir_all(tmp_dir)
    }
}
