use serde::Deserialize;
use std::env::current_dir;
use std::process::Child;
use std::{
    fmt::Display,
    fs,
    path::{Path, PathBuf},
    process::Command,
};

use crate::packager::PackageBuilder;

/// Configuration for the whole building process. Specify how to
/// run the build process, run tests and then create one or multiple
/// zip files with the result and additional files.
///
#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields, rename_all = "kebab-case")]
pub struct Config {
    #[serde(default = "default_out_dir")]
    pub out_dir: String,
    packages: Option<Vec<Package>>,
    pub build: Option<Build>,
    pub test: Option<Test>,
}

fn default_out_dir() -> String {
    "out/".to_string()
}

#[derive(Debug, Deserialize)]
pub struct Build {
    cmd: String,
    #[serde(default)]
    args: Vec<String>,
    pub out: PathBuf,
    pub dst: PathBuf,
}

impl Build {
    pub fn execute(&self) -> std::io::Result<Child> {
        let cd: PathBuf = current_dir().unwrap();
        let args: Vec<String> = self
            .args
            .iter()
            .map(|arg| arg.replace("!CD!", cd.to_str().unwrap_or(".")))
            .collect();
        Command::new(&self.cmd).args(args).spawn()
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Test {
    cmd: String,
    #[serde(default)]
    args: Vec<String>,
    pub allow_fail: bool,
}

impl Test {
    pub fn execute(&self) -> std::io::Result<Child> {
        let cd: PathBuf = current_dir().unwrap();
        let args: Vec<String> = self
            .args
            .iter()
            .map(|arg| arg.replace("!CD!", cd.to_str().unwrap_or(".")))
            .collect();
        Command::new(&self.cmd).args(args).spawn()
    }
}

/// Package with a name and file information to add to the package
#[derive(Debug, Deserialize)]
pub struct Package {
    pub name: String,
    files: Vec<FileInfo>,
}

impl From<Package> for PackageBuilder {
    fn from(val: Package) -> Self {
        PackageBuilder::from(val.files, val.name)
    }
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields, untagged)]
pub enum FileInfo {
    Path { source: PathBuf, dest: PathBuf },
    Glob(String),
}

#[derive(Debug)]
pub enum Error {
    ParseConfigError(String, String),
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::ParseConfigError(msg, path) => write!(f, "Error reading: {} {}", msg, path),
        }
    }
}

impl Config {
    pub fn new(path: impl AsRef<Path>) -> Result<Config, Error> {
        Self::_new(path.as_ref())
    }

    fn _new(path: &Path) -> Result<Config, Error> {
        match toml::from_str(&fs::read_to_string(path).expect("Error reading config file")) {
            Ok(value) => Ok(value),
            Err(e) => Err(Error::ParseConfigError(
                e.to_string(),
                path.to_str().unwrap().to_string(),
            )),
        }
    }

    pub fn iter_packages(&mut self) -> Vec<Package> {
        self.packages.take().unwrap_or_else(Vec::new)
    }
}
