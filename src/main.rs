use std::env;

use termion::color;

use crate::{config::Config, packager::PackageBuilder};

#[macro_use]
extern crate clap;

mod cli;
mod config;
mod packager;

fn main() {
    env_logger::init();
    let matches = clap_app!(pkg_builder =>
        (version: "0.1.0")
        (author: "Sandro Covo <sandro@covo.ch>")
        (about: "Utility to build zip files from a pkg folder and a Build.toml configuration.")
        (@arg reference: -r --reference [HASH] +takes_value
         "specify git-reference to create a build including only files changed since then.")
        (@arg out_dir: -o --output [PATH] +takes_value "Where to place the created zip files.")
        (@arg full_prefix: -f --full [STR] +takes_value
         "If specified a full build will always be created with the given prefix.")
        (@arg skip_build: -s --skip "Skip build and test commands.")
    )
    .get_matches();

    if let Some(dir) = matches.value_of("dir") {
        env::set_current_dir(dir).expect("Failed to open directory");
    }

    let mut config = Config::new("Build.toml".to_owned()).expect("Could not read config file");

    if !matches.is_present("skip_build") {
        if let Some(build) = &config.build {
            println!("Starting build");
            build
                .execute()
                .expect("Failed to start build cmd")
                .wait()
                .expect("Build failed");
            println!(
                "{}Build successful{}",
                color::Fg(color::Green),
                color::Fg(color::Reset)
            );
        };

        if let Some(test) = &config.test {
            println!("Running tests");
            let result = test
                .execute()
                .expect("Failed to start test")
                .wait()
                .expect("Running tests failed.");
            if !result.success() && !test.allow_fail {
                println!("allow_fail: {}", test.allow_fail);
                println!(
                    "{}Tests failed, exiting{}",
                    color::Fg(color::Red),
                    color::Fg(color::Reset)
                );
                panic!("ABORT...");
            }
        }
    }

    for package in config.iter_packages() {
        println!("Start Packaging \"{}\"", package.name);
        let mut copier: PackageBuilder = package.into();

        if let Some(full) = matches.value_of("full_prefix") {
            if let Some(build) = &config.build {
                copier.add_path(&env::current_dir().unwrap().join(&build.out), &build.dst);
            };
            let result = copier.build(
                matches
                    .value_of("out_dir")
                    .or(Some(&config.out_dir))
                    .unwrap(),
                Some(full),
            );
            println!(
                "{}Built package {}{}",
                color::Fg(color::Green),
                result.display(),
                color::Fg(color::Reset)
            );
        };

        if let Some(tag) = matches.value_of("reference") {
            copier.filter_reference(tag);
        }

        if let Some(build) = &config.build {
            copier.add_path(&env::current_dir().unwrap().join(&build.out), &build.dst);
        };

        let result = copier.build(
            matches
                .value_of("out_dir")
                .or(Some(&config.out_dir))
                .unwrap(),
            None,
        );
        println!(
            "{}Built package {}{}",
            color::Fg(color::Green),
            result.display(),
            color::Fg(color::Reset)
        );
    }
}
