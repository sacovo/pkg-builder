# PKG Builder

## Configuration

Put a `Build.toml` in the root of your repository.

```toml

# specify the directory to put the zip files into.
out-dir = "out/"


# Configure how to build the project
[build]
# Command to build the source
cmd = "gcc"

# Specify arguments for the executable, 
args = ["-Wall", "main.c"]

# Where the output is stored
out = "build/test_file"
# Where it should land in the zip file
dst = "/bin/main"

# Command to run the tests
[test]
# See build
cmd = "echo"
args = ["Hello World"]

# Allow the test to fail
allow-fail = true


# Specify as many packages as you need
[[packages]]
# Name used for the zip file
name = "WithDelete"
# Files from pkg/ to include in the zip
files = [
  # Either as mapping from pkg/ to the location in the zip
  {source="hello.txt", dest="/bin/hello.txt"},  # pkg/hello.txt becomes /bin/hello.txt
  # Or as a glob, in this case the path relative from pkg/ will become the absolute path
  "etc/**/*" # => copy the structure from pkg/etc to /etc
  "usr/bin/*.sh"# => only .sh files
]


# Add more packages
[[packages]]
name = "WithoutDelete"
files = [
  {source="hello2.txt", dest="/bin/hello.txt"},  # pkg/test/hello.txt => /test/hello.txt
  "etc/**/*"
]

```